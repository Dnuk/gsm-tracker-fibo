Tracker GPS/GSM. 

Przesyła pozycję wiadomościami SMS w formacie linku do mapy Google.

Wersja dla modemu GSM Fibocom G510 /moduł produkcji Mikrotar
http://mikrotar.pl/produkty/gsm-g510/

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

----------------------------------------------------------------------------------

Język C, środowisko Atmel Studio 6.x, procesor Atmega644PA.

-- Uwaga - kompilacja w Atmel Studio 7+ uniemożliwi ponowne przeniesienie projektu do starszej wersji.

Może też wystąpić błąd z funkcją init();. Należy używać jej wtedy jako INLINE.

Schematy, pliki PCB i dokumentacja (częściowo nieaktualna):

https://bitbucket.org/Dnuk/gondolav2-hardware-repo/overview

------------------------------------

Kontakt do nas oraz więcej materiałów tutaj:   https://www.facebook.com/DNFsystems/
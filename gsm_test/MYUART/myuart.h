

#ifndef MYUART_H_
#define MYUART_H_

#define UART0_BAUD 19200 // gps
#define __UBRR_0 (  F_CPU / 16 / UART0_BAUD - 1   )

#define UART1_BAUD 57600      // GSM
#define __UBRR_1 (  F_CPU / 16 / UART1_BAUD - 1   )


void uart0_init( uint16_t ubrr);
//void uart_zmien_baud( uint16_t ubrr);
void uart0_putc( uint8_t data );
void uart0_puts( char * s );
void uart0_puts_noCR( char * s );
//void uart_puts_P(const char *s);
void uart0_putlong( uint32_t liczba, uint8_t radix);

uint8_t USART0_Receive( void);


//////////////////////// DLA UK�ADU Z DWOMA UARTAMI

void uart1_init( uint16_t ubrr);
//void uart_zmien_baud( uint16_t ubrr);
void uart1_putc( uint8_t data );
void uart1_puts( char * s );
void uart1_puts_noCR( char * s );
//void uart_puts_P(const char *s);
void uart1_putlong( uint32_t liczba, uint8_t radix);

uint8_t USART1_Receive( void);

#endif /* MYUART_H_ */

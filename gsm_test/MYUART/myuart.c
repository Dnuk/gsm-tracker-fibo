

#include "avr/io.h"
#include "stdlib.h"
#include "avr/pgmspace.h"
#include "myuart.h"



void uart0_init( uint16_t ubrr)
{
/*Set baud rate */
UBRR0H = (uint8_t)(ubrr>>8);
UBRR0L = (uint8_t)ubrr;
/*Enable transmitter and/or receiver */
UCSR0B = (1<<RXEN0)|(1<<TXEN0); // przerwanie odbioru[juz nie], odbior, nadawanie
}



void uart0_putc( uint8_t data )
{
// Wait for empty transmit buffer
while( !( UCSR0A & (1<<UDRE0)) );
// Put data into buffer, sends the data
UDR0 = data;
}

void uart0_puts( char * s )   // wysy�anie string'ow
{
	while( *s ) uart0_putc( *s++ );
	uart0_putc( 13 ); // dodano powr�t karetki
}

void uart0_puts_noCR( char * s )   // wysy�anie string'ow
{
	while( *s ) uart0_putc( *s++ );
}

/*
void uart_puts_P(const char *s)  // wysy�anie z pami�ci flash
{
	register char c;
	while ((c = pgm_read_byte( s++) )) uart_putc(c);
}
*/

void uart0_putlong( uint32_t liczba, uint8_t radix)  // wysylanie liczb z pamieci
{
	char buf[17];
	ltoa( liczba, buf, radix);
	uart0_puts( buf );
}

uint8_t USART0_Receive( void)
{
	/* Wait for data to be received*/
	while( !(UCSR0A & (1<<RXC0)) );
	/* Get and return received data from buffer*/
	return UDR0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DLA UK�ADU Z DWOMA UARTAMI


void uart1_init( uint16_t ubrr)
{
//Set baud rate
UBRR1H = (uint8_t)(ubrr>>8);
UBRR1L = (uint8_t)ubrr;
//Enable transmitter and/or receiver
UCSR1B = (1<<RXEN1)|(1<<TXEN1); // przerwanie odbioru[juz nie], odbior, nadawanie
}


void uart1_putc( uint8_t data )
{
// Wait for empty transmit buffer
while( !( UCSR1A & (1<<UDRE1)) );
// Put data into buffer, sends the data
UDR1 = data;
}


void uart1_puts( char * s )   // wysy�anie string'ow
{
	while( *s ) uart1_putc( *s++ );
	uart1_putc( 13 );
}

void uart1_puts_noCR( char * s )   // wysy�anie string'ow
{
	while( *s ) uart1_putc( *s++ );
}


void uart1_putlong( uint32_t liczba, uint8_t radix)  // wysylanie liczb z pamieci
{
	char buf[17];
	ltoa( liczba, buf, radix);
	uart1_puts( buf );
}

uint8_t USART1_Receive( void)
{
	/* Wait for data to be received*/
	while( !(UCSR1A & (1<<RXC1)) );
	/* Get and return received data from buffer*/
	return UDR1;
}


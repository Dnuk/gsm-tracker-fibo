/*
 * gsm_test.c
 *
 * Created: 2015-12-05 20:17:25
 *  Author: Dan
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include <string.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "MYUART/myuart.h"

#define LED_ON PORTD |= (1<<5)
#define LED_OFF PORTD &= ~(1<<5)

#define uart0_interrupt_enable UCSR0B |= (1<<RXCIE0)
#define uart0_interrupt_disable UCSR0B &= ~(1<<RXCIE0)
#define uart1_interrupt_enable UCSR1B |= (1<<RXCIE1)
#define uart1_interrupt_disable UCSR1B &= ~(1<<RXCIE1)

#define  uart0_available (UCSR0A & (1<<RXC0))
#define  uart1_available (UCSR1A & (1<<RXC1))

void PWRKEY();
void send_sms();
void dzwon();
void Konwersja_Pozycji();
inline void czysc_buf_wysokosc();
void execute_standby_flag();
void Przepisz_ost_dane();
inline void czysc_buf_szer_dlug();

uint8_t send_command(char command[], char response[], uint16_t timeout);

//int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout);


volatile static uint16_t licznik = 0;
volatile static uint16_t prog_timer_sek = 0;
volatile static uint8_t prog_timer_sek_1 = 0;
volatile static uint8_t prog_timer_sek_2 = 0;
//volatile static uint16_t prog_timer_sek_fix = 0;
volatile static uint16_t milisek = 0;
volatile static uint16_t delay_timer = 0;

char String[83];
volatile static uint8_t inc = 0;
char Pozycja[30];
char Wysokosc[8];
char CzasUTC[7];
char Fix[2];
char Sat[3];

volatile static char Szerokosc[15];
volatile static char Dlugosc[15];
//char Szer_pocz[4];
//char Dlug_pocz[4];
//double Szer_liczba_pocz = 0; // zamienione na zmienne wewn.
//double Dlug_liczba_pocz = 0;
//double Szerokosc_liczba = 0;
//double Dlugosc_liczba = 0;

volatile static char Ostatn_Szerokosc[15];
volatile static char Ostatn_Dlugosc[15];
volatile static char Ostatn_Wysokosc[8];
volatile static char Ostatn_CzasUTC[7];

volatile static uint8_t inc2 = 0;
char String2[51];
volatile static uint8_t flaga_modem_rx = 0;

uint8_t czasowka_sms = 45;

uint8_t flaga_modem_standby = 0; // 0-modem pracuje; 1-tryb uspienia
uint16_t licznik_sms = 0; 

void init()
{
	wdt_enable(WDTO_8S);
	
	DDRA = 0xFF;
	DDRB = 0xFF;
	DDRC = 0xFF;
	DDRD = 0b11101111; // POWERKEY na poczatku jako wejscie oraz stan niski ( hi-impedance [odciety] )
	
	PORTA = 0x00;
	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x00;
	
	// TIMER2 W TRYBIE CTC
	OCR2A = 14;  // 14745600/1024=4400 >> 4400/14= 1028.5714..Hz
	TCCR2A |= (1<<WGM21); //tryb ctc
	TCCR2B |= (1<<CS22) | (1<<CS21) | (1<<CS20);    // preskaler TIMER2 1024
	TIMSK2 |= (1<<OCIE2A);
	
	uart0_init(__UBRR_0);		// inicjalizacja UART'u >>>> GPS
	uart1_init(__UBRR_1);		// >> GSM
	
	uart0_interrupt_enable;		// odblokuj przerwania dla GPS	
	uart1_interrupt_enable;		// oraz dla GSM
	
	sei();
	//dzwon(); // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}

ISR (TIMER2_COMPA_vect) // obsluga przerwania od TIMER2
{
	licznik++;      // zmienna liczaca 1028 przerwan dla ~1 sekundy
	milisek++;
	delay_timer++;

	if (licznik >= 1028)// fizyczna liczba przerwan ktore uplyna zanim wykonasz dzialanie  // w tym wypadku ~1s
	{
		prog_timer_sek++;
		prog_timer_sek_1++;
		prog_timer_sek_2++;
		//prog_timer_sek_fix++;
		licznik = 0;
	}
}

ISR(USART0_RX_vect) // >> GPS
{
		volatile unsigned char tmp = UDR0; // przepisz  znak z rejestru do zmiennej (nie, nie da sie bezposrednio)
		if (tmp != 10) { String[inc] = tmp; inc++; } // jesli odebrany znak jest rozny od <LF>=10 lub <CR>=13 to dopisz go do stringa
		else // jesli jednak wystapil taki znak, to znaczy ze skonczyl odbierac stringa
		{

			if ( String[3] == 'G' && String[4] == 'G' && String[5] == 'A' )
			{
				for(uint8_t i = 17; i <= 42; i++) Pozycja[i-17] = String[i];
				for(uint8_t i = 54; i <= 60 && String[i] != '.'; i++) Wysokosc[i-54] = String[i];
				for(uint8_t i = 7; i <= 12; i++) CzasUTC[i-7] = String[i];
				Sat[0] = String[46]; Sat[1] = String[47];
			}

			else if ( String[3] == 'G' && String[4] == 'S' && String[5] == 'A' ) Fix[0] = String[9];

			inc = 0;			//	na koniec zeruj zmienna inkrementujaca tablice stringa (inc)
		}
}


ISR(USART1_RX_vect)  // >> GSM
{
	uart0_interrupt_disable;
	volatile unsigned char tmp2 = UDR1;	
	//if (inc2 > 50) inc2 = 0;
	if (tmp2 != 13)
	 { 
		String2[inc2] = tmp2;
		inc2++;
	 }
	else
	{
		flaga_modem_rx = 1;
		inc2 = 0;
	}
	uart0_interrupt_enable;	
}

//--------------------------------------------------------------
int main(void)
{
	init();
	_delay_ms(2000);
	
	LED_ON;
	if( send_command("AT","OK",1000) == 0 ) PWRKEY(); // wysyla AT, jesli nie odbierze OK w ciagu 1s to wlacz GSM
	LED_OFF;

    while(1)
    {
		wdt_reset();
		
		
		//if (Fix[0] == '2' || Fix[0] == '3') prog_timer_sek_fix = 0;
		
		//if(prog_timer_sek_fix >= 1200) flaga_modem_standby = 1;
		//else flaga_modem_standby = 0;
			
		if (prog_timer_sek_2 >= 2) // sprawdzaj co 2s
		{
			if (strstr(String2, "RING") != NULL) // czy w buforze GSM jest RING (ktos dzwoni)
			{
				_delay_ms(100);
				for(uint8_t i = 0; i<=8; i++) String2[i] = 0; // kasuj "RING" z bufora by nie wysylal sms'a ponownie
				uart1_puts("ATH");		// jesli tak to odrzuc polaczenie 
				_delay_ms(2000);		// poczekaj
				//send_sms();				// i wyslij sms >> tak na hama powodowalo bledy <<<<<
				prog_timer_sek = 245; // ustawienie  czasu do wyslania nastepnego sms
			}
			
			PORTB ^= (1<<6); // neguj stan na pinie MOSI - generowanie zbocza narastajacego - reset zewnetrznego watchdoga
			prog_timer_sek_2 = 0;
		}

		
		if(prog_timer_sek >= czasowka_sms)		// wyslij sms z pozycja co zadany czas
		{
			LED_ON;
			Konwersja_Pozycji();
			//Przepisz_ost_dane();
			send_sms();
			czysc_buf_wysokosc();
			czysc_buf_szer_dlug();// <<<<<!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//dzwon();
			LED_OFF;
			prog_timer_sek = 0;		
		}
		
		if (prog_timer_sek_1 >= 30) // okresowo sprawdzaj warunki pracy (co 25s)
		{
			if (atoi(Wysokosc) >= 5000) flaga_modem_standby = 1; // uspij modem powyzej 5km
			else flaga_modem_standby = 0; // wybudz modem ponizej 5km
			
			if (Fix[0] == '2' || Fix[0] == '3')
			{ 
				czasowka_sms = 120;
				Konwersja_Pozycji();
				Przepisz_ost_dane();
			}
			else czasowka_sms = 240;
									
			execute_standby_flag(); // wykonuje wlaczenie/wylaczenie modemu gsm zaleznie od flagi rzadania
				
			prog_timer_sek_1 = 0;
		}
				
    }
}
//---------------------------------------------------------------



void PWRKEY()
{
	PORTD &= ~(1<<4); //stan niski
	DDRD |= (1<<4); //wyjscie	
	_delay_ms(1000);	
	PORTD |= (1<<4); // pin zostaje jako wyjscie ale ma stan wysoki (przycisk puszczony)
}

void send_sms()
{
	licznik_sms++;
	char buf_licz_sms[6];
	itoa(licznik_sms, buf_licz_sms, 10);
	
	if(Fix[0] == '2' || Fix[0] == '3')
	{	
		
	uart1_puts( "AT+CMGF=1" );
	_delay_ms(50);		
	uart1_puts( "AT+CMGS=\"+48531993700\"" );
	_delay_ms(50);

	uart1_puts_noCR("#");
	uart1_puts_noCR(buf_licz_sms);
	uart1_puts_noCR(" Wys:");
	uart1_puts_noCR(Wysokosc);
	uart1_putc('M');
	uart1_putc(' ');
	uart1_puts_noCR("UTC:");
	uart1_puts_noCR(CzasUTC);
	uart1_puts_noCR(";  ");
	
	uart1_puts_noCR("https://www.google.com/maps?q=");
	uart1_puts_noCR(Szerokosc);
	uart1_putc(',');
	uart1_puts_noCR(Dlugosc);
	uart1_puts_noCR("&z=14");
	
	_delay_ms(10);
	
	uart1_putc(26); // (znak SUB == ctrl+z) wyslanie sms'a
	}
	
	else
	{
	//uart1_puts("AT");

	uart1_puts( "AT+CMGF=1" );
	_delay_ms(50);
	uart1_puts( "AT+CMGS=\"+48531993700\"" );
	_delay_ms(50);
	
	uart1_puts_noCR("#");
	uart1_puts_noCR(buf_licz_sms);
	uart1_puts_noCR(" <NoFix> ");
	uart1_puts_noCR("Wys:");
	uart1_puts_noCR(Ostatn_Wysokosc);
	uart1_putc('M');
	uart1_putc(' ');
	uart1_puts_noCR("UTC:");
	uart1_puts_noCR(Ostatn_CzasUTC);
	uart1_puts_noCR(";  ");
	
	uart1_puts_noCR("https://www.google.com/maps?q=");
	uart1_puts_noCR(Ostatn_Szerokosc);
	uart1_putc(',');
	uart1_puts_noCR(Ostatn_Dlugosc);
	uart1_puts_noCR("&z=14");
	
	_delay_ms(10);
	
	uart1_putc(26); // (znak SUB == ctrl+z) wyslanie sms'a
	}
}

void dzwon()
{
	if( send_command("ATD+48531993700","OK",1000) == 0 )
	{
		_delay_ms(2000);
		_delay_ms(2000);
		uart1_puts( "ATH");
	}
	//uart1_puts(" ATD+48531993700 ");
	
}

void Przepisz_ost_dane() // jesli jest fix, zapisz dane na pozniej w osobnych zmiennych
{
	if(Fix[0] == '2' || Fix[0] == '3')
	{
		for (uint8_t i = 0; i <= 15; i++) Ostatn_Szerokosc[i] = Szerokosc[i];
		for (uint8_t i = 0; i <= 15; i++) Ostatn_Dlugosc[i] = Dlugosc[i];
		for (uint8_t i = 0; i <= 8; i++) Ostatn_Wysokosc[i] = Wysokosc[i];
		for (uint8_t i = 0; i <= 7; i++) Ostatn_CzasUTC[i] = CzasUTC[i];
	}
}

void Konwersja_Pozycji()	// zamiana pozycji gps z formatu protokolu NMEA na format dziesietny
{
	char Szer_pocz[4];
	char Dlug_pocz[4];
	volatile static double Szerokosc_liczba = 0;
	volatile static double Dlugosc_liczba = 0;
	volatile static double Szer_liczba_pocz = 0;
	volatile static double Dlug_liczba_pocz = 0;
	
	for(uint8_t i = 0; i <= 7; i++) Szerokosc[i] = Pozycja[i+2]; // przepisz z pozycji tylko dalsza czesc szerokosci geo
	for(uint8_t i = 0; i <= 7; i++) Dlugosc[i] = Pozycja[i+16];  // przepisz sama dalsza czesc dlugosci geo
	Szer_pocz[0]=Pozycja[0]; Szer_pocz[1]=Pozycja[1];			// przepisz tylko stopnie szerokosci
	Dlug_pocz[0]=Pozycja[13]; Dlug_pocz[1]=Pozycja[14];	Dlug_pocz[2]=Pozycja[15]; // przepisz tylko stopnie dlugosci

	 Szer_liczba_pocz = atof(Szer_pocz);			// konwersja stringow na liczby zmiennoprzecinkowe
	 Dlug_liczba_pocz = atof(Dlug_pocz);			//
	 Szerokosc_liczba = atof(Szerokosc);			//
	 Dlugosc_liczba = atof(Dlugosc);				//

	Szerokosc_liczba = Szerokosc_liczba/60.0 + Szer_liczba_pocz; // przeliczenie na pozycje w formacie decymentalnym
	Dlugosc_liczba = Dlugosc_liczba/60.0 + Dlug_liczba_pocz;

	dtostrf(Szerokosc_liczba,0,5,Szerokosc);		// konwersja spowrotem do stringa
	dtostrf(Dlugosc_liczba,0,5,Dlugosc);			// dla wygody uzyto zmiennych z poczatku funkcji [szerokosc i dlugosc]

	Szerokosc[strlen(Szerokosc)] = Pozycja[11];
	Dlugosc[strlen(Dlugosc)] = Pozycja[25];
}

inline void czysc_buf_wysokosc() // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<,
{
	for(uint8_t i = 0; i<=8; i++) Wysokosc[i] = 0;
}

inline void czysc_buf_szer_dlug() // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<,
{
	for(uint8_t i = 0; i<=15; i++) Szerokosc[i] = 0;
	for(uint8_t i = 0; i<=15; i++) Dlugosc[i] = 0;
}


uint8_t send_command(char command[], char response[], uint16_t timeout)
{
	uint8_t result = 0; // zeruj zmienna z wynikiem komunikacji
	milisek = 0; // zeruj czas timeout'u
	uart1_puts(command); // wyslij komende
	while (1) // czekaj na deklarowana odpowiedz
	{
		if(milisek >= timeout){ break;} //  wyjdz z petli
		if(flaga_modem_rx == 1)
		{
			if (strstr(String2, response) != NULL){result = 1; break;} // jesli komenda wlasciwa wyjdz z petli bez ruszania gsm
		}
	}
	return result;
}

void execute_standby_flag()
{
	uint8_t stan = send_command("AT","OK",1000); // sprawdz jednorazowo stan modemu
	
	if (stan == 0 && flaga_modem_standby == 0) // gdy modem nie odpowiada i chcemy by dzialal
	{										   // jesli tak to go wlacza
		PWRKEY();
	}
	
	if (stan == 1 && flaga_modem_standby == 1) // gdy modem odpowiada a nie chcemy by dzialal
	{										   // jesli tak to go wlacza
		PWRKEY();
	}
}

